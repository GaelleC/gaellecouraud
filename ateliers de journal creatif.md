---
title: "Ateliers de Journal Créatif"
order: 3
in_menu: true
---
Qu'est-ce que le journal Créatif ? 

J'organise des ateliers en présentiel et en ligne sur de nombreuses thématiques. 

Vous êtes une structure d'accompagnement des personnes ? Je peux réaliser des ateliers sur mesure sur les thématiques souhaitées. 

Si vous souhaitez découvrir le journal créatif, je vous invite à vous inscrire au prochain atelier de découverte en ligne. 