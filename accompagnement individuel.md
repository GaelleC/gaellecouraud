---
title: "Accompagnement Individuel"
order: 2
in_menu: true
---
Je propose de vous accompagner dans votre évolution de façon individuelle et globale via le coaching, l’hypnose, le journal créatif et mon expérience.

Séance de 45mn à 1h. 